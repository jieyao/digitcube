English | [简体中文](./README.md)

**This page was translated by Google**

<h1 align="center">
<b>DigitalCube Development Kit</b>
</h1>
<div align="center">Digitcube is a simple and easy-to-use visual large-screen front-end framework, developed based on vue2</div>

<p align="center">
   <a href="https://doc.ayin86.com/">Documentation</a> •
   <a href="https://ayin86.com/">Demo Site</a> •
   <a href="https://www.npmjs.com/package/digitcube-core">Core library</a> •
   <a href="https://www.youtube.com/@ayin86cn">Youtube</a>
</p>


## 🛡️ Declaration

**On March 16, 2023, the core file of the development kit was upgraded to 1.4, and there are some destructive updates, please download the development kit again.** 

**Previous version development kits can be used normally if the core library and language packs are kept in their original versions. If the previous development package is upgraded to the latest 1.4 core, it will not work properly.**

**<span style="color:red">The Temporary license built into this development kit is only used for testing, experience and learning. Unable to debug, unable to package and deploy. </span>**

The files in this development kit are development source codes, which can be used for commercial project development only by updating the license. For details, please contact the administrator.



- Free users can learn data visualization development ideas, design, themes, multi-language, style, layout, and the use of echarts through this development kit
- Authorized users, contact the administrator to update the license and go directly to the development stage.



**This development kit has 5 built-in data screens, as shown in the figure below.**

**Data Screen A**-Support map drill-down data linkage

<img src="./demo/screenA.png" style="border-radius:10px" width="800" />

 **Data Screen C**

<img src="./demo/screenC.png" style="border-radius:10px" width="800" />

 **Data Screen D**

<img src="./demo/screenD.png" style="border-radius:10px" width="800" />

 **Data Screen A-Mobile** 

<img src="./demo/screenA-mobile.png" style="border-radius:10px" width="300" />


 **enterprise splash screen 4x2**

<img src="./demo/splicingScreens.png" style="border-radius:10px" width="800" />

**This development kit has 3 built-in functional interfaces, namely 3D panel, tech border and chart colour matching tool**

 **3D panel**

<img src="./demo/board3D.png" style="border-radius:10px" width="800" />

 **Tech border**

<img src="./demo/techBorder.png" style="border-radius:10px" width="800" />

 **Chart Colour Matching Tool**

<img src="./demo/chartPalette.png" style="border-radius:10px" width="800" />



**For the following data screens and more demos, please check out [Digital Cube Demo Site](https://ayin86.com/)** 

<img src="./demo/screenB.png" style="border-radius:10px" width="800" />


-----

## ✨Features

- **🖥️ Full Ended Adaptation**

  The perfect all-end adaptation solution for PCs, mobile phones, tablets, corporate splicing screens, so to speak, any terminal device can be perfectly adapted, the industry's leading adaptation solution.

- **🎨 Powerful built-in themes**

  One click to switch styles, all elements, elements and details of everything are perfectly supported theme switching. Themes can be developed quickly and customised according to customer requirements using the built-in theme designer.

- **🛸Free development without limitations**

  Using webpack, vue2 and other popular technology stack, by calling self-developed components and chart components, you can quickly deploy online without too much tedious development process, greatly shortening the development cycle. Truly free development at source level.

- **🧩De-bitmap to fully use SVG**

  Full vectorisation (de-bitmap), due to the special use scenario of visualisation of large screens, the use of traditional bitmap situation graphics zoom details blurred, while the use of vector graphics details scaled can still maintain the original details clarity.

- **📊Enterprise splicing screen**

  In the face of the enterprise splicing screen, we have a very large number of display solutions, according to the customer's splicing screen equipment situation for custom development. The best display effect can be achieved under any equipment.


- **🌈 Intelligent colour matching for charts**

  With this framework, you can say goodbye to the headache of colour matching for charts. It provides the ability to colour match charts intelligently and the chart colouring tool.

- **🗺️GEO 3D Maps**

  Based on the GEO Json format, the map is easy to use and can meet most of the scenarios, you can place any data on the map such as points, lines, surfaces and graphics. The map can be presented in a flat or 3D format.

- **🧑‍ Internationalisation**

  Aimed at global business and expanding market coverage, internationalisation can improve user satisfaction, reduce development and maintenance costs and increase the scalability of the software.

- **🚀Steady iterations for rapid response**

  The framework has gone through two major releases and numerous minor iterations, with timely bug fixes and steady feature expansion. For paid users can be achieved in a timely response.


-----

## 📜Update Notes

- **20230316 The core is upgraded to 1.4, and a system title component is added, as well as a lot of detailed modifications.**

- **20230302 The development source code of mobile terminal data screen and splicing screen is integrated into the development kit.**
- **20230225 Documentation update English and Traditional Chinese language**
- **20230223 Release the development kit to Gitee and Github for the first time, and the documents are released simultaneously**



A free version may be released, without the features of themes, multilingualism and with fewer components. For free commercial use, the list of components offered is as follows (preliminary):

- Adaptive components (isometric mode only)
- Chart components
- TechBorder (about 10-12)
- TechTitle
- BlockTitle
- loading
- SVG backgrounds

These components are basically sufficient for an average commercial project, as for other features, they are only available in the premium version. The free version allows you to add any three-way component or custom component to implement other features.

The time frame is uncertain, after all the author is developing it in his spare time, it will probably be released in 3-6 months.




-----

## 📖 Installation Tutorial

1. `npm i` installation dependencies
2. `npm run serve` to start the project to preview

Please refer to [documentation](https://doc.ayin86.com/) for development package description and usage

-----

## 🌟Licensing

Due to policy reasons, please add my instant messaging contact to the [Github@digitcube](https://github.com/ayin86/digitcube) profile for customers outside of mainland China!



For a description of the license, please refer to [License Description](https://doc.ayin86.com/en-US/license/license#Introduce)



Or contact me via email at ayin86cn@gmail.com I will check my emails once every 3-5 days
